# dtkX

...

## Android build

The following examples use fish shell syntax.

### Arch example

Assuming `yay` is the package manager and `AUR` is set up.

``` fish
yay -S jre8-openjdk
```

Download `android-studio` and `android-ndk` and extract it to respective folders
in `Development`.

Launch `android-studio` and install proposed SDK in `~/Development/android-sdk`.

``` fish
set -Ux JAVA_HOME /usr/lib/jvm/java-8-openjdk/jre

set -Ux ANDROID_HOME $HOME/Development/android-sdk
set -Ux ANDROID_SDK $HOME/Development/android-sdk
set -Ux ANDROID_NDK $HOME/Development/android-ndk
set -Ux ANDROID_NDK_TOOLCHAIN_ROOT $ANDROID_NDK/toolchains
set -Ux ANDROID_NATIVE_API_LEVEL 29

set -x Qt5_DIR $HOME/Development/qt/5.13.1/android_armv7

set -x PATH $Qt5_DIR/bin $PATH
```

``` fish
cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/dtkCrossAND.toolchain.cmake -DANDROID_PLATFORM=android-29 .. -DANDROID_ABI=armeabi-v7a
```

Possible ABIs for Android NDK toolchain: armeabi-v7a, arm64-v8a, x86, x86_64

#### Building for release 

If you want to publish your app on the Google play store, there are a couple of
requirements you should meet: 

- Your app should be signed and compiled in release 
- You need to provide apk's for 32bit and 64bit devices 

To sign your app simply add the options:

``` fish
cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/dtkCrossAND.toolchain.cmake -DANDROID_PLATFORM=android-29 .. -DANDROID_ABI=armeabi-v7a -DDTKCROSS_ANDROID_ALIAS <keystore alias> -DDTKCROSS_ANDROID_KEYSTORE <url/to/keystore> 
```

Remember that the 32 and 64 bit versions need different version codes. This can
be specified by using the `QT_ANDROID_APP_VERSION_VODE` variable.

Also note that in order for the Google Play Console to accept your APKs, you
need to make sure that the version code of the 64 bit version is higher than the
32 bit (otherwise the 64 bit version is shadowed). 

Currently working for a new build system which will automatically create an app
bundle. Stay tuned ! 

#### App Icon and name

This directly stems from the AndroidManifest.xml file. You should modify the
following fields : 

``` xml
<application [...] android:label="@QT_ANDROID_APP_NAME@" android:icon="@drawable/icon">

</application>
```

For the app name, just specifiy the QT_ANDROID_APP_NAME cmake variable. 

For the icons: 
In your res folder after the build, you should find three folders 

``` fish
ls build/res 
drawable-hdpi/  drawable-ldpi/  drawable-mdpi/
```

These correspond to the different DPI versions of your app logo. In order for
these to be correctly populated, make sure to copy the corresponding png files
in the assets/app-logos folder. 

#### Adding other app resources 

For the curious reader, or someone who wishes to add other resources to their
app using this build system: Qt uses the build_directory/package folder to store
resources that should be added before building for android. Files stored in this
folder will then be moved to the build_directory/res. 
The reader is refered to
https://developer.android.com/guide/topics/resources/providing-resources for
more info. 

#### Since QT 5.14.1

With Qt 5.14.1 the build system has changed and we can now create Android App
Bundles (aab). 

First of all, the android directory has changed : 
``` fish
set -x Qt5_DIR $HOME/Development/qt/5.14.1/android
```

The android build types are specified directly in Cmake. 

For lack of a better solution, at the moment packaging several abis in one
bundle requires to first build each android lib separately. 

Ex for armeabi-v7a and arm64-v8a (and the pit application) :
``` fish
cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/dtkCrossAND.toolchain.cmake -DANDROID_PLATFORM=android-21 .. -DANDROID_ABI=arm64-v8a -DQT_ANDROID_APP_NAME=... -DQT_ANDROID_APP_ICONS_PATH=/path/to/app/assets/app-icons/app
make pit 
cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/dtkCrossAND.toolchain.cmake -DANDROID_PLATFORM=android-21 .. -DANDROID_ABI=armeabi-v7a -DQT_ANDROID_APP_NAME=... -DQT_ANDROID_APP_ICONS_PATH=~/path/to/app/assets/app-icons/app
make pit 
cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/dtkCrossAND.toolchain.cmake -DANDROID_PLATFORM=android-21 .. -DQT_ANDROID_APP_NAME=... -DQT_ANDROID_APP_ICONS_PATH=/path/to/app/assets/app-icons/app -DANDROID_BUILD_ABI_armeabi-v7a=ON -DANDROID_BUILD_ABI_arm64-v8a=ON
make pit_aab
```

### MacOS example

Assuming `brew` is the package manager.

``` fish
brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk8
```

``` fish
sdkmanager "platforms;android-21"
```

``` fish
set -Ux JAVA_HOME /Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home

set -Ux ANDROID_HOME /usr/local/share/android-sdk
set -Ux ANDROID_SDK /usr/local/share/android-sdk
set -Ux ANDROID_NDK /usr/local/share/android-ndk
set -Ux ANDROID_NDK_TOOLCHAIN_ROOT $ANDROID_NDK/toolchains
set -Ux ANDROID_NATIVE_API_LEVEL 21

set -x PATH $ANDROID_SDK/tools $PATH
set -x PATH $HOME/Development/qt/5.13.1/android_armv7/bin $PATH
```

``` fish
cmake -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake -DANDROID_PLATFORM=android-21 ..
```

## iOS build

The following examples use fish shell syntax.

### MacOS example

``` fish
set -x Qt5_DIR $HOME/Development/qt/5.13.1/ios
set -x PATH $Qt5_DIR/bin $PATH
```

``` fish
cmake -G Xcode -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/dtkCrossIOS.toolchain.cmake -DPLATFORM=OS64COMBINED ..
cmake --build . --config Release

open /Applications/Xcode.app/Contents/Developer/Applications/Simulator.app

xcrun simctl list
xcrun simctl boot 40C37F6D-ED8B-4D6C-850A-674E6338B999
xcrun simctl install booted bin/XXX.app
xcrun simctl launch booted XXX.app
xcrun simctl terminate booted XXX.app
xcrun simctl uninstall booted bin/XXX.app
xcrun simctl shutdown 40C37F6D-ED8B-4D6C-850A-674E6338B999
```

## Web build

First, install the emscripten toolchain.

``` fish
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
./emsdk install sdk-1.38.27-64bit
./emsdk activate --embedded sdk-1.38.27-64bit
source emsdk_env.fish
```

```
Adding directories to PATH:
PATH += $HOME/Development/emsdk
PATH += $HOME/Development/emsdk/fastcomp/emscripten
PATH += $HOME/Development/emsdk/node/12.9.1_64bit/bin

Setting environment variables:
EMSDK = $HOME/Development/emsdk
EM_CONFIG = $HOME/.emscripten
EMSDK_NODE = $HOME/Development/emsdk/node/12.9.1_64bit/bin/node
```

``` fish
set -x Qt5_DIR $HOME/Development/qt/5.13.1/wasm_32
set -x PATH $Qt5_DIR/bin $PATH
cmake -DCMAKE_TOOLCHAIN_FILE=/path/to/dtk-cross/toolchains/web.toolchain.cmake -DEMSCRIPTEN_ROOT_PATH=$HOME/Development/emsdk/emscripten/1.38.27 -Wno-dev ..
cd public; emrun main.html
```
